-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 11, 2020 at 09:58 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `yahoo_mail`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_details`
--

CREATE TABLE `admin_details` (
  `user_id` varchar(255) NOT NULL,
  `authority` varchar(255) DEFAULT NULL,
  `email_id` varchar(255) DEFAULT NULL,
  `is_active` bit(1) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin_details`
--

INSERT INTO `admin_details` (`user_id`, `authority`, `email_id`, `is_active`, `name`, `password`) VALUES
('b9893ee2-2e38-43a4-af63-695a9c750c7c', 'ROLE_ADMIN', 'kgovind144@gmail.com', b'1', 'Govind Kumar', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `admin_email_records`
--

CREATE TABLE `admin_email_records` (
  `send_to` varchar(255) NOT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin_email_records`
--

INSERT INTO `admin_email_records` (`send_to`, `subject`, `message`) VALUES
('kgovind14471@gmail.com', 'Good night', 'sweet dream'),
('kgovind144@gmail.com', 'Good night', 'sweet dream');

-- --------------------------------------------------------

--
-- Table structure for table `user_email_records`
--

CREATE TABLE `user_email_records` (
  `send_to` varchar(255) NOT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_email_records`
--

INSERT INTO `user_email_records` (`send_to`, `subject`, `message`) VALUES
('kgovind14471@gmail.com', '', 'summer day'),
('kgovind144@gmail.com', 'Good night', 'sweet dream'),
('nishashaw193@gmail.com', '', 'summer day'),
('shravannayak16@gmail.com', '', 'summer day');

-- --------------------------------------------------------

--
-- Table structure for table `user_signup_details`
--

CREATE TABLE `user_signup_details` (
  `user_id` varchar(255) NOT NULL,
  `birthday` date DEFAULT NULL,
  `email_id` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_signup_details`
--

INSERT INTO `user_signup_details` (`user_id`, `birthday`, `email_id`, `name`, `status`) VALUES
('34a063be-5bd5-4544-a77d-11a090a8e605', '1997-06-22', 'kgovind14471@gmail.com', 'Govind Nayak', b'1'),
('f97c6903-f363-40c4-a4fb-82aed44a09b0', '1998-09-28', 'nishashaw193@gmail.com', 'Nisha Shaw', b'1'),
('ff3a0d72-7f75-4829-822e-c170474df477', '1987-08-16', 'shravannayak16@gmail.com', 'Shravan Nayak', b'1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_details`
--
ALTER TABLE `admin_details`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `admin_email_records`
--
ALTER TABLE `admin_email_records`
  ADD PRIMARY KEY (`send_to`);

--
-- Indexes for table `user_email_records`
--
ALTER TABLE `user_email_records`
  ADD PRIMARY KEY (`send_to`);

--
-- Indexes for table `user_signup_details`
--
ALTER TABLE `user_signup_details`
  ADD PRIMARY KEY (`user_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
