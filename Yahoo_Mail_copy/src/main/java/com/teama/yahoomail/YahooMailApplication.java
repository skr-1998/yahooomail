package com.teama.yahoomail;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.teama.yahoomail.repository.AdminRepository;

@SpringBootApplication
@EnableScheduling
@EnableJpaRepositories(basePackageClasses = AdminRepository.class)
public class YahooMailApplication {

	public static void main(String[] args) {
		SpringApplication.run(YahooMailApplication.class, args);
	}

}
