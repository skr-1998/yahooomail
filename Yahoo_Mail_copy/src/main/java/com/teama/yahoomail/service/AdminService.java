package com.teama.yahoomail.service;

import java.util.Optional;

import com.teama.yahoomail.entity.Admin;


public interface AdminService {
	

	boolean isAdmin(String id);

	String saveAdmin(Admin admin);
	
	boolean isAdminMail(String mail);
	
	Optional<Admin> getAdmin(String adminMail);

}	
