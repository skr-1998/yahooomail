package com.teama.yahoomail.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.teama.yahoomail.entity.Admin;

public interface AdminRepository extends JpaRepository<Admin, String> {
	public Optional<Admin> findByEmail(String email);
}
